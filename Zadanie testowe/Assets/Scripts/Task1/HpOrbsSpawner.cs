using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HpOrbsSpawner : MonoBehaviour
{
    public static UnityAction<Vector3> orbSpawned;

    [SerializeField] private HpOrb hpOrbPrefab;

    [Range(5, 20)]
    [SerializeField] private float nextSpawnTime;

    private float currentTimeToSpawn;

    private void Start()
    {
        currentTimeToSpawn = nextSpawnTime;
    }

    private void Update()
    {
        if (currentTimeToSpawn > 0)
        {
            currentTimeToSpawn -= Time.deltaTime;

        }
        else if (currentTimeToSpawn < 0)
        {
            SpawnOrb();

            currentTimeToSpawn = nextSpawnTime;
        }
    }

    private void SpawnOrb()
    {
        var randomPos = new Vector3(Random.Range(-30, 30), 1.5f, Random.Range(-30, 30));
        Instantiate(hpOrbPrefab, randomPos, Quaternion.identity, gameObject.transform);
        orbSpawned?.Invoke(randomPos);
    }
}
