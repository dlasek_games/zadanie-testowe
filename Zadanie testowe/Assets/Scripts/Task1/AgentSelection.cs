using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentSelection : MonoBehaviour
{
    private AgentView selectedAgent;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.collider.CompareTag("Agent"))
                {
                    if(selectedAgent != null)
                    {
                        selectedAgent.ChangeMaterialForDefault();
                    }

                    selectedAgent = hit.collider.GetComponent<AgentView>();
                    selectedAgent.ChangeColorForSelected();

                    var agentController = hit.collider.GetComponent<AgentController>();
                    var agentLives = agentController.Model.currentLives;
                    AgentInfoCanvas.instance.InitializeCanvasInfo(agentLives, agentController.Name);
                }
                else
                {
                    if(selectedAgent != null)
                    {
                        selectedAgent.ChangeMaterialForDefault();
                        AgentInfoCanvas.instance.HideCanvasInfo();
                    }
                }
            }
        }
    }

}
