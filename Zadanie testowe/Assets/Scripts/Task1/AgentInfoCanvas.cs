using TMPro;
using UnityEngine;

public class AgentInfoCanvas : MonoBehaviour
{
    public static AgentInfoCanvas instance;

    [SerializeField] private TextMeshProUGUI hpText;
    [SerializeField] private TextMeshProUGUI nameText;

    private void Awake()
    {
        instance = this;
    }

    public void InitializeCanvasInfo(int agentHP, string agentName)
    {
        ShowCanvasInfo();
        nameText.text = string.Format("Name: " + agentName);
        hpText.text = string.Format($"HP: {agentHP}");
    }

    private void ShowCanvasInfo()
    {
        hpText.gameObject.SetActive(true);
        nameText.gameObject.SetActive(true);
    }

    public void HideCanvasInfo()
    {
        hpText.gameObject.SetActive(false);
        nameText.gameObject.SetActive(false);
    }
}
