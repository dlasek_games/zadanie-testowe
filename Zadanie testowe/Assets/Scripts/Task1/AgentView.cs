using UnityEngine;

public class AgentView : MonoBehaviour
{
    [SerializeField] private Material defaultMaterial;

    private AgentModel agentModel;

    private AgentController agentController;
    private Renderer renderer;

    private void Start()
    {
        agentController = GetComponent<AgentController>();
        renderer = GetComponent<Renderer>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        var controller = collision.gameObject.GetComponent<AgentController>();

        if(controller != null)
        {
            agentController.OnCollision();
        }
    }

    public void ChangeMaterialForDefault()
    {
        renderer.material = defaultMaterial;
    }

    public void ChangeColorForSelected()
    {
        renderer.material.color = Color.red;
    }
}
