using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentSpawner : MonoBehaviour
{
    public static AgentSpawner instance;

    [SerializeField] private AgentView agentPrefabView;
    public AgentView AgentPrefabView { get => agentPrefabView; }

    [SerializeField] private Transform spawnPoint;

    [Range(0, 30)]
    [SerializeField] private int agentMaxCounter;

    [Range(2, 10)]
    [SerializeField] private float nextSpawnTime;

    private float currentTimeToSpawn;

    private int currentAgentCounter;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        currentAgentCounter = 0;
        currentTimeToSpawn = nextSpawnTime;
    }

    private void Update()
    {
        if(currentAgentCounter < agentMaxCounter && currentTimeToSpawn > 0)
        {
            currentTimeToSpawn -= Time.deltaTime;
            
        }
        else if(currentTimeToSpawn < 0)
        {
            SpawnNextAgent();

            currentAgentCounter += 1;
            currentTimeToSpawn = nextSpawnTime;
        }
    }

    private void SpawnNextAgent()
    {
        Instantiate(AgentPrefabView, spawnPoint.position, Quaternion.identity, spawnPoint.transform);
    }

    public void DecreaseCurrentAgentCounter()
    {
        currentAgentCounter -= 1;
    }
}
