using System;
using UnityEngine;

[Serializable]
public class AgentModel
{
    [SerializeField] private int moveSpeed;
    [SerializeField] private int walkRadius = 100;

    public int currentLives;

    public enum names
    {
        Bogdan,
        Jasiu,
        Zdzisiek,
        Genowefa,
        Tadek,
        Danuta,
        Kaziu,
        Leszek,
        Maja,
        Judyta,
        Szymon
    }

    public int MoveSpeed
    {
        get => moveSpeed;
    }

    public int WalkRadius
    {
        get => walkRadius;
    }

    public string GetRandomName()
    {
        var name = (names)UnityEngine.Random.Range(0, Enum.GetNames(typeof(names)).Length);
        return name.ToString();
    }

    public void SetRandomSpeed()
    {
        moveSpeed = UnityEngine.Random.Range(6, 20);
    }

    public void SetHpPoints(int newHp)
    {
        currentLives = newHp;
    }
}
