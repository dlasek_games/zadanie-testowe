using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HpOrb : MonoBehaviour
{
    public static UnityAction orbTaken;

    private void OnCollisionEnter(Collision collision)
    {
        orbTaken?.Invoke();
        Destroy(gameObject);
    }
}
