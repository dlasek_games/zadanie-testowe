using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentController : MonoBehaviour
{
    [SerializeField] private AgentModel model;

    public AgentModel Model
    {
        get => model;
    }

    private NavMeshAgent agent;

    private string name;

    public string Name
    {
        get => name;
    }

    private bool isOrbActive;

    private void OnEnable()
    {
        HpOrbsSpawner.orbSpawned += MoveTowardOrb;
        HpOrb.orbTaken += SetFullHp;
    }

    private void OnDisable()
    {
        HpOrbsSpawner.orbSpawned -= MoveTowardOrb;
        HpOrb.orbTaken -= SetFullHp;
    }

    private void Start()
    {
        name = model.GetRandomName();
        agent = GetComponent<NavMeshAgent>();

        model.SetRandomSpeed();

        if(agent != null)
        {
            agent.speed = model.MoveSpeed;
            agent.SetDestination(RandomNavMeshLocation());
        }
    }

    private void Update()
    {
        if(agent != null && agent.remainingDistance <= agent.stoppingDistance && !isOrbActive)
        {
            agent.SetDestination(RandomNavMeshLocation());
        }
    }

    public void OnCollision()
    {
        ResetAgentDestination();
        TakeDamage();
    }

    public void TakeDamage()
    {
        model.currentLives -= 1;

        if (model.currentLives < 0)
        {
            AgentSpawner.instance.DecreaseCurrentAgentCounter();
            Destroy(gameObject);
        }
    }

    public void ResetAgentDestination()
    {
        agent.isStopped = true;
        agent.ResetPath();
    }

    private Vector3 RandomNavMeshLocation()
    {
        Vector3 finalPos = Vector3.zero;
        Vector3 randomPos = Random.insideUnitSphere * model.WalkRadius;

        randomPos += transform.position;
        if (NavMesh.SamplePosition(randomPos, out NavMeshHit hit, model.WalkRadius, 1))
        {
            finalPos = hit.position;
        }

        return finalPos;
    }

    private void SetFullHp()
    {
        model.SetHpPoints(3);
        isOrbActive = false;
        ResetAgentDestination();
    }

    private void MoveTowardOrb(Vector3 orbPos)
    {
        isOrbActive = true;
        ResetAgentDestination();
        agent.SetDestination(orbPos);
    }
}

