using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DivisibilityRule : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI areaText;

    public void Write100Numbers()
    {
        string stringToWrite = "";
        
        for(int i = 1; i < 101; i++)
        {
            if(i % 3 == 0 && i % 5 == 0)
            {
                stringToWrite += " MarkoPolo";
            }else if(i % 3 == 0)
            {
                stringToWrite += " Marko";
            }else if(i % 5 == 0)
            {
                stringToWrite += " Polo";
            }
            else
            {
                stringToWrite += string.Format($" {i}");
            }
        }

        areaText.text = stringToWrite;
    }
}
